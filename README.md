# ICPUTRD Overview #

### What is this repository for? ###

* Summary, describe key other repos: platform and tags

### Contribution guidelines ###

We welcome external contributors: if you use platfor or other software, please 
rais issue in the related repo. If you want to use ICPUTRD with ARF data, please contact
Prof. Dawnie Steadman. 

### Who do I talk to? ###

* audris, osteo at utk.edu are Principal Investigators for this project 
* mousavi at vols.utk.edu is primary AI specialist

### Publications and Grants ###

Image segmentation:

[Machine-assisted annotation of forensic imagery](https://arxiv.org/pdf/1902.10848.pdf)


Image labeling and clustering:

[Collaborative Learning of Semi-Supervised Clustering and Classification for Labeling Uncurated Data](https://arxiv.org/pdf/2003.04261.pdf)

[An Analytical Workflow for Clustering Forensic Images](https://www.aaai.org/Papers/AAAI/2020GB/SA-MousaviS.568.pdf)

[SChISM: Semantic Clustering via Image Sequence Merging for Images of Human-Decomposition](https://drive.google.com/file/d/1xeFKQoXbaMJyfeAC0CHNvzL4GTVNKI0U/view?usp=sharing)

